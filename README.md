# Spectrum Next Tilemap Demo

This is a simple example of a tilemap text display for the Spectrum Next (https://www.specnext.com) written in Z80 assembly. The demo consists of four modules with some code as noted below from other authors. This code is free to use without restrictions where the original authors allow (from what I know all 3rd part-code used was license-free).


## utils.asm
  Next utilities
  
  Original code from ZX Spectrum Next demos by Mike Dailly - http://dailly.blogspot.com  
  Modified by Thomas Sturm, https://sturm.to - https://toot.site/@tsturm

The demo only uses a small part of these utilities, but I've included the complete file since it's quite useful.

## font.asm
  Spectrum Next Tilemap Font
  
  Created by Thomas Sturm, https://sturm.to - https://toot.site/@tsturm

I've created this font from scratch and it's not necessarily complete or done. Feel free to improve it in your own projects. :) 

## tiletool.asm
  Spectrum Next Tilemap Utilities
  
  Based on https://github.com/next-dev/ed/blob/master/src/screen.s  
  Extended and modified by Thomas Sturm, https://sturm.to - https://toot.site/@tsturm

This file has several utilities for tilemap management and text rendering. 

## demo.asm
  Spectrum Next Tilemap Example Code

  Created by Thomas Sturm, https://sturm.to - https://toot.site/@tsturm

This is the main demo file - the code assembles without warnings in sjasmplus (https://github.com/z00m128/sjasmplus/)

The demo initialies tilemap mode and then allows you to scroll up and down in a short text with the "A" and "Z" keys.

