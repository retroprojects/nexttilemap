; *****************************************************************************************************************************
;	Spectrum Next Tilemap Utilities
; 
;	Based on https://github.com/next-dev/ed/blob/master/src/screen.s
;	Extended and modified by Thomas Sturm, https://sturm.to - https://toot.site/@tsturm
; 
; 	This code is free to use without restrictions
; 
;	v1.0
; 
;	Assembler directives are for sjasmplus - https://github.com/z00m128/sjasmplus/
; 
; *****************************************************************************************************************************



;;----------------------------------------------------------------------------------------------------------------------
;; create a tilemap-compatible font file from the 8x8 bit-level font in font.asm


fontSetup:
				ld		de,RawFont
				ld		hl,TrueFont
				add		hl,$0400		; add 32x32 bytes to tilemap font start (leave out the first 32 control ASCII characters)

				ld 		b,$5f			; number of characters in current raw font set

@fontLoop
				push	bc
				
				ld		b,8

@fontByteLoop
				ld		a,(de)
				inc		de
				; each bit of the raw font translates to 4 bits in the true font

				ld		c,0
				bit		7,a
				jr		z,@fontBit6
				ld		c,$10
@fontBit6
				bit		6,a
				jr		z,@fontBit5
				inc		c
;				add		bc,$0004
@fontBit5
				ld		(hl),c
				inc		hl
				ld		c,0
				bit		5,a
				jr		z,@fontBit4
				ld		c,$10
@fontBit4
				bit		4,a
				jr		z,@fontBit3
				inc		c
;				add		bc,$0004
@fontBit3
				ld		(hl),c
				inc		hl
				ld		c,0
				bit		3,a
				jr		z,@fontBit2
				ld		c,$10
@fontBit2
				bit		2,a
				jr		z,@fontBit1
				inc		c
;				add		bc,$0004
@fontBit1
				ld		(hl),c
				inc		hl
				ld		c,0
				bit		1,a
				jr		z,@fontBit0
				ld		c,$10
@fontBit0
				bit		0,a
				jr		z,@fontByteDone
				inc		c
;				add		bc,$0004
@fontByteDone
				ld		(hl),c
				inc		hl

				djnz	@fontByteLoop

				pop		bc
				djnz	@fontLoop
				
				ret


;;----------------------------------------------------------------------------------------------------------------------
;; Video modes

InitTilemap:
                xor     a
                out     (254),a

        ; Initialise palette
        ;
        ;   0   Normal text
        ;   1   Cursor
        ;   2   Inverse text
        ;
                ld      bc,$000f        ; Paper/ink combination (white on black)
                xor     a               ; For slot 0
                call    SetColour
                ld      bc,$0700        ; Paper/ink combination (black on white)
                ld      a,1             ; For slot 1
                call    SetColour
                ld      bc,$0a0f        ; Paper/ink combination (bright white on bright red)
                ld      a,2
                call    SetColour

                call    ClearScreen

        ; Set up the tilemap
        ; Memory map:
        ;       $4000   80x32x2 tilemap (5120 bytes)
        ;       $6000   128*32 tiles (4K)
        ;
                nextreg $07,2                   ; Set speed to 14Mhz
                nextreg $6b,%11000001           ; Tilemap control
												; bit 7: 0=disable tilemap, 1=enable tilemap
												; bit 6: 0=40x32, 1=80x32
												; bit 5: 0=attributes in tilemap, 1=no attributes in tilemap
												; bit 4: 0=primary palette, 1=secondary palette
												; bits 3-2: reserved (0)
												; bit 1: 0=256 tile mode, 1=512 tile mode
												; bit 0: 1=tilemap over ULA
                
                nextreg $6e,$00                 ; Tilemap base offset   00 00 0000	=> bank 5 + 0 => $4000
                nextreg $6f,$20                 ; Tiles base offset     00 10 0000  => bank 5 + $2000 => $6000
                
                nextreg $4c,8                   ; Transparency colour (bright black)
                nextreg $68,%10000000           ; Disable ULA output

DoneVideo:
                ret


;;----------------------------------------------------------------------------------------------------------------------
;; Palette control

Palette:
                db  %00000000
                db  %00000010
                db  %10000000
                db  %10000010
                db  %00010000
                db  %00010010
                db  %10010000
                db  %10010010
                db  %01101101	; 8 bright black ?
                db  %00000011
                db  %11100000
                db  %11100011
                db  %00011100
                db  %00011111
                db  %11111100
                db  %11111111

SetColour:
        ; Input:
        ;       B = Paper
        ;       C = Ink
        ;       A = Slot (0-15)
        ; Uses:
        ;       HL, DE, BC, A
                nextreg $43,%00110000   ; Set tilemap palette
                sla     a
                sla     a
                sla     a
                sla     a
                nextreg $40,a
                ; Paper colour
                ld      de,Palette
                ld      l,b
                ld      h,0
                add     hl,de
                ld      a,(hl)
                nextreg $41,a
                ; Ink colour
                ld      de,Palette
                ld      l,c
                ld      h,0
                add     hl,de
                ld      a,(hl)
                nextreg $41,a
                ret



;;----------------------------------------------------------------------------------------------------------------------
;; Utitlies

CalcTileAddress:
        ; Input:
        ;       B = Y coord (0-31)
        ;       C = X coord (0-79)
        ; Output:
        ;       HL = Tile address
                push    bc
                push    de
                ld      e,b
                ld      d,80
                mul                 ; DE = 80Y
                ex      de,hl       ; HL = 80Y
                ld      b,0
                add     hl,bc       ; HL = 80Y+X
                add     hl,hl       ; 2 bytes per tilemap cell
                ld      bc,$4000    ; Base address of tilemap
                add     hl,bc
                pop     de
                pop     bc
                ret




;;----------------------------------------------------------------------------------------------------------------------

TilePrintBuffer:
        ; Input
        ;       DE = buffer start 
        ; Output:
        ;       
        ; Uses:
        ;       BC, HL, DE, A
; this writes out the current buffer until a 0 ends the buffer or until 28x80 characters (4480 bytes) 
; have been written to the tilemap starting at 2/0
; This routine can handle carriage returns. If it encounters a code 13, it will clear the rest of the current line and continue
; writing out characters in the next line.

				ld 		hl,cursorPos
				ld		(hl),0		; store start x cursor position

; start coordinates of the display area
				ld		b,2			; y		- in our demo we leave the top two lines permanent and re-write the other 28 lines every cycle
				ld		c,0			; x
				ld		a,0			; color
				
        ; Calculate tilemap address (each tile is 2 bytes)
                call    CalcTileAddress
                ld 		bc,hl
				add 	bc,4480		; end address in tilemap
				push	bc

                add     a,a
                add     a,a
                add     a,a
                add     a,a
                ld      c,a			; c is now color

.tpbMainLoop    ld      a,(de)		; load the next character from the text buffer
                and     a
                jp      z,.tpbfinish	; if this is a 0, finish printing
                
                cp		13
				jr		z,.tpbcr	; it's a 13 - deal with carriage return
                
                ld      (hl),a      ; Write out character
                inc     hl
                ld      a,c
                ld      (hl),a      ; Write out attribute
                inc     hl

.tpbcrret		pop		bc			; check if we have reached the end of the text display area
				push	af				
				call	cmpgte		; HL >= BC ?
				jr		c,.tpbcont1
				pop		af
				ret

.tpbcr
; manage carriage return
; pull cursor position
; diff = (80 - cursor)*2
; add diff to de
; reset cursor position to 0
; de: current text buffer position
; hl: next tilemap address
; cursorPos: x-position of the next character on screen 0-79
				push	de
				push	hl

				ld 		hl,cursorPos
				ld		d,(hl)
				ld		a,80
				sub		d

				ld		d,a			; store the distance to the right edge

				ld		a,255
				ld		(hl),a		; reset the cursor position to -1

				pop		hl

; loop through d
				push	bc
				ld		b,d
				
.clearline		ld		a,32
                ld      (hl),a     ; Write out space
                inc     hl
                ld      a,c
                ld      (hl),a      ; Write out attribute
                inc     hl
				djnz	.clearline
				pop		bc

				
;				ld		a,d
;				sla		a 			; multiply by 2 - each tilemap character is 2 bytes
;				add		hl,a


				
				pop		de
				
                ld      a,c
				jr		.tpbcrret


.tpbcont1		
				; here, a is color, and bc is end-of-display, de is current character in buffer and hl is next address in tilemap
				push	hl			; at this point af and hl are on the stack
				ld 		hl,cursorPos
				ld		a,79		; check if the cursor for the latest written character is at the last x position
				sub 	(hl)
			   	ld		a,(hl)
			   	jr 		nz,.tpbcont2

				ld		a,0			; reset cursor (it was 79)
				jr		.tpbcont3

.tpbcont2		inc		a			; next cursor position
.tpbcont3		ld		(hl),a
				pop		hl
				pop		af
				
				push	bc			; stash end of display
				ld		c,a			; return color to c
                inc     de
                jr      .tpbMainLoop

.tpbfinish		
				; we've encountered a 0, so we fill the rest of the map with spaces
				pop		bc			; address of the end of the text display area
.tpbfinishLoop
                ld      (hl),32      ; Write out character
                inc     hl
                ld      a,c
                ld      (hl),a      ; Write out attribute
                inc     hl

				call	cmpgte		; HL >= BC ?
				jr		c,.tpbfinishLoop
				ret

cursorPos		db		0





;;----------------------------------------------------------------------------------------------------------------------

TilePrintString:
        ; Input
        ;       B = Y coord (0-31)
        ;       C = X coord (0-79)
        ;       DE = string
        ;       A = colour (0-15)
        ; Output:
        ;       DE = points after string
        ; Uses:
        ;       BC, HL, DE, A

        ; Calculate tilemap address
                call    CalcTileAddress
                add     a,a
                add     a,a
                add     a,a
                add     a,a
                ld      c,a
.l1             ld      a,(de)
                and     a
                jr      z,.finish
                ld      (hl),a      ; Write out character
                inc     hl
                ld      a,c
                ld      (hl),a      ; Write out attribute
                inc     hl
                inc     de
                jr      .l1
.finish         inc     de
                ret




;;----------------------------------------------------------------------------------------------------------------------

ClearScreen:
        ; Clear screen (write spaces in colour 0 everywhere)
        ; Uses:
        ;       BC, HL, A
                ld      bc,2560
                ld      hl,$4000
.l1             ld      a,' '
                ld      (hl),a
                inc     hl
                xor     a
                ld      (hl),a
                inc     hl
                dec     bc
                ld      a,b
                or      c
                jr      nz,.l1
                ret