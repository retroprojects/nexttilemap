; ************************************************************************
;
; 	Next utilities - original code from ZX Spectrum Next demos by Mike Dailly - http://dailly.blogspot.com
;	Modified by Thomas Sturm, https://sturm.to - https://toot.site/@tsturm
; 
; 	This code is free to use without restrictions
; 
;	v1.1
; 
; ************************************************************************

; ************************************************************************
;
;	Function:	Set current bank - upto 1MB supported			
;	In:		A = bank to set, paged into $c000
;
;	$7FFD:	D0  |
;			D1  |-  bank (128k)
;			D2  |
;			D3  - ULA screen select
;			D4  - rom
;			D5  - disable paging
;			D6  - unused
;			D7  - unused
;
;	$DFFD:	D0  - 256k
;			D1  - 512k
;			D2  - 1024k
;			D3  - unused
;			D4  - unused
;			D5  - unused
;			D6  - unused
;			D7  - unused
;
;			128K Memory map uses 16K banks. 
;
;	      	  0xffff + -------- + -------- + -------- + -------- + -------- + -------- + -------- + -------- + -------- + -------- +
;	      	     	 |  Bank 0  |  Bank 1  |  Bank 2  |  Bank 3  |  Bank 4  |  Bank 5  |  Bank 6  |  Bank 7  |          |
;	      	     	 |          |          |          |          |          |          |          |          |          |
;	slot 7	     	 |          |          | (also at |          |          | (also at |          |          |  NEXT RAM------->
;	      		     |          |          |  0x8000) |          |          |  0x4000) |          |          |          |
;	      	     	 |          |          |          |          |          |  screen  |          |          |          |
;	slot 6	  0xc000 + -------- + -------- + -------- + -------- + -------- + -------- + -------- + -------- + -------- + -------- +
;		           	 |  Bank 2  |            Any one of these pages may be switched in.
;		             |          |
;	slot 5		     |          |
;		           	 |          |
;		           	 |          |
;	slot 4	  0x8000 + -------- +
;		           	 |  Bank 5  |
;		             |          |
;	slot 3	     	 |          |
;	      	     	 |          |
;	      	     	 |  screen  |
;	slot 2	  0x4000 + -------- + -------- +
;		             |  ROM 0   |  ROM 1   | Either ROM may be switched in.
;		             |          |          |
;	slot 1     	     |          |          |
;		             |          |          |
;		             |          |          |
;	slot 0	  0x0000 + -------- + -------- +
;
;
;
; ************************************************************************
ResetBank:		xor		a					; bank 0 sits at $C000
SetBank:		ld		(CurrentBank),a
				add		a,a
				nextreg	$56,a
				inc 	a
				nextreg	$57,a
				ld		a,(CurrentBank)
				ret
CurrentBank:	db		0



; ************************************************************************
;
;	Function:	wait until two frames have passed (to limit app to 25 frames/second)
;	
;
; ************************************************************************
waitFrame:		
				push 	hl

				ld 		hl,pretim       ; previous time setting
waitFrameLoop	ld 		a,(FRAMES)		; current timer setting from Spectrum Frame Counter
				sub 	(hl)            ; difference between the two.
			   	cp 		2               ; have two frames elapsed yet?
			   	jr 		z,waitFrameGo	; yes, no more delay.
			   	jp 		waitFrameLoop
waitFrameGo:	ld 		a,(FRAMES)		; current timer.
       			ld 		(hl),a          ; store this setting.

                pop		hl
       			ret
pretim: 		db 		0





; ******************************************************************************
; helper function 16-bit CMPGTE -> test if HL>=BC
; Input      HL, BC
; Output     CF=0 -> true
;            CF=1 -> false
; uses		 A
; ******************************************************************************
cmpgte  		push 	hl
				ld 		a,h
        		xor 	b
        		jp 		m,cmpgte2
        		sbc 	hl,bc
        		jr 		nc,cmpgte3
cmpgte1			scf						;false
;        		ld hl,0
				pop		hl
        		ret
cmpgte2 		bit		7,b
        		jr 		z,cmpgte1
cmpgte3 		or 		a				;true
;        		ld hl,1
				pop		hl
        		ret




; ******************************************************************************
; Function:	Scan Kempston joysticks
; ******************************************************************************
ReadJoysticks:
				push 	bc
                ld      bc, Kempston1
				in		a,(c)
				pop		bc
				ret




; ******************************************************************************
; Function:	Scan the whole keyboard
; ******************************************************************************
ReadKeyboard:
				ld		b,39
				ld		hl,Keys
				xor		a
@clearKeys:		ld		(hl),a			; clear Keys buffer
				inc		hl
				djnz	@clearKeys

				ld		ix,Keys
				ld		bc,$fefe		;Caps,Z,X,C,V
				ld		hl,RawKeys
@ReadAllKeys:	in		a,(c)
				ld		(hl),a
				inc		hl		
		
				ld		d,5
				ld		e,$ff
@DoAll:			srl		a
				jr		c,@notset
				ld		(ix+0),e
@notset:		inc		ix
				dec		d
				jr		nz,@DoAll
	
				ld		a,b
				sla		a
				jr		nc,ExitKeyRead
				or		1
				ld		b,a
				jp		@ReadAllKeys
ExitKeyRead:
				ret


; half row 1
VK_CAPS		equ	0
VK_Z		equ	1
VK_X		equ	2
VK_C		equ	3
VK_V		equ	4

; half row 2
VK_A		equ	5
VK_S		equ	6
VK_D		equ	7
VK_F		equ	8
VK_G		equ	9

; half row 3
VK_Q		equ	10
VK_W		equ	11
VK_E		equ	12
VK_R		equ	13
VK_T		equ	14

; half row 4
VK_1		equ	15
VK_2		equ	16
VK_3		equ	17
VK_4		equ	18
VK_5		equ	19

; half row 5
VK_0		equ	20
VK_9		equ	21
VK_8		equ	22
VK_7		equ	23
VK_6		equ	24

; half row 6
VK_P		equ	25
VK_O		equ	26
VK_I		equ	27
VK_U		equ	28
VK_Y		equ	29

; half row 7
VK_ENTER	equ	30
VK_J		equ	31
VK_L		equ	32
VK_K		equ	33
VK_H		equ	34

; half row 8
VK_SPACE	equ	35
VK_SYM		equ	36
VK_M		equ	37
VK_N		equ	38
VK_B		equ	39

Keys:		ds	40
RawKeys		ds	8






; ******************************************************************************
; 
; Function:	Upload a set of sprite patterns to the pattern memory
; In:		E = sprite shape to start at
;			D = number of sprites
;			HL = shape data
;
; ******************************************************************************
UploadSpritePattern
                ld      a,e		; get start shape
                ld		e,0		; each pattern is 256 bytes, now de is a 256-byte block counter

                ld      bc, $303B
                out     (c),a

                ; upload ALL sprite image data
                ld      bc, SpritePattern		; $5b
@UpLoadSprite:           
                outinb			; out (c),(hl), hl++

                dec     de
                ld      a,d
                or      e
                jr      nz, @UpLoadSprite
                ret









; ************************************************************************
;
;	Function:	Clear the 256 colour screen to a set colour
;	In:		A = colour to clear to ($E3 makes it transparent)
;
; ************************************************************************
Cls256:
				push	bc
				push	de
				push	hl

				ld		d,a			; byte to clear to
                ld		e,3			; number of blocks
                ld		a,1			; first bank... (bank 0 with write enable bit set)
                
                ld      bc, $123b                
@LoadAll:		out		(c),a			; bank in first bank
                push	af
                
                                
                ; Fill lower 16K with the desired byte
                ld		hl,0
@ClearLoop:		ld		(hl),d
                inc		l
                jr		nz,@ClearLoop
                inc		h
                ld		a,h
                cp		$40
                jr		nz,@ClearLoop

                pop		af			; get block back
                add		a,$40
                dec		e			; loops 3 times
                jr		nz,@LoadAll

                ld      bc, $123b		; switch off background (should probably do an IN to get the original value)
                ld		a,0
                out		(c),a     

                pop		hl
                pop		de
                pop		bc
                ret                          



; ************************************************************************
;
;	Function:	Clear the spectrum attribute screen
;	In:			A = attribute
;	Attribute: 	[F][B][BG2][BG1][BG0][FG2][FG1][FG0]
; 	Colors: 	Black, Blue, Red, Magenta, Green, Cyan, Yellow, White
;				000    001   010  011      100    101   110     111
; ************************************************************************
ClsATTR:
				push	hl
				push	bc
				push	de

                ld      ($5800),a
                ld      hl,$5800
                ld      de,$5801
                ld      bc,1000
                ldir

                pop		de
                pop		bc
                pop		hl
                ret



; ************************************************************************
;
;	Function:	clear the normal spectrum screen
;
; ************************************************************************
Cls:
				push	hl
				push	bc
				push	de

				xor		a
                ld      ($4000),a
                ld      hl,$4000
                ld      de,$4001
                ld      bc,6143
                ldir

                pop		de
                pop		bc
                pop		hl
                ret



; ************************************************************************
;
;	Function:	Enable the 256 colour Layer 2 bitmap
;
; ************************************************************************
BitmapOn:
                ld      bc, $123b
                ld	a,2
                out	(c),a     
                ret                          

               	
; ************************************************************************
;
;	Function:	Disable the 256 colour Layer 2 bitmap
;
; ************************************************************************
BitmapOff:
                ld      bc, $123b
                ld	a,0
                out	(c),a     
                ret          




; ************************************************************************
;
;	Function:	Set ULA attributes in a row
;	a	= attribute value
;	b	= how many attributes
;	d	= x
;	e	= y
;
;	Attribute byte: F  B  P2 P1 P0 I2 I1 I0
;	42: bright, black paper, red ink
;	05: dark, black paper, cyan ink
;
;	color sequence: 
;	black		000
;	blue		001
;	red			010
;	magenta		011
;	green		100
;	cyan		101
;	yellow		110
;	white		111
;
; ************************************************************************
SetAttributes:
				push	bc
				push	de
				push	hl
				
				ld		hl,$5800	; start of ULA attributes
				push	de
				ld		d,32
				mul
				add		hl,de
				pop		DE
				ld		e,d
				ld		d,0
				add		hl,de

@setAttrLoop:
				ld      (hl),a
				inc		hl
				djnz	@setAttrLoop

				pop 	hl
				pop		de
				pop		bc
				ret




; ******************************************************************************
;
; 	Get screen address
; 	B 	= 	Y pixel position
; 	C 	= 	X character position
; 	Returns address in HL
;
; ******************************************************************************
GetPixelAddress:
				push	af
				push 	BC
				push	de
				
				LD 		A,B        	 ; Calculate Y2,Y1,Y0
				AND 	%00000111   ; Mask out unwanted bits
				OR 		%01000000    ; Set base address of screen
				LD 		H,A          ; Store in H
				LD 		A,B          ; Calculate Y7,Y6
				RRA           		  ; Shift to position
				RRA
				RRA
				AND		%00011000   ; Mask out unwanted bits
				OR 		H            ; OR with Y2,Y1,Y0
				LD 		H,A          ; Store in H
				LD 		A,B          ; Calculate Y5,Y4,Y3
				RLA           		  ; Shift to position
				RLA
				AND 	%11100000   ; Mask out unwanted bits
				LD 		L,A          ; Store in L
				LD 		A,C          ; Calculate X4,X3,X2,X1,X0
				RRA           		  ; Shift into position
				RRA
				RRA
				AND		%00011111   ; Mask out unwanted bits
				OR 		L            ; OR with Y5,Y4,Y3
				LD 		L,A          ; Store in L
				
				pop		de
				pop		bc
				pop		af
				RET





; ******************************************************************************
;
;	Print String to ULA screen
;	HL  = address of begin of string - the string has to end with a 0 byte
;	DE  = address to print to (normal spectrum screen)
;
; ******************************************************************************
PrintString:	
			push	bc
			push	hl
			push	af
			ld		bc,AlphaCharset

@pStrLoop:
			ld		a,(hl)
			cp		0
			jr		z,@finishpStr
			inc		hl
			cp		32		; space
			jr		nz,@azChar
;			inc		e		; skip one character since it was a space
;			jr		@nextChar
			ld		a,92	; our space
@azChar
			sub		65
			push	hl
			call	DrawCharacter
			pop 	hl
@nextChar
			jr		@pStrLoop

@finishpStr
			pop		af
			pop		hl
			pop		bc
			ret





; ******************************************************************************
;
;	A   = hex value to print
;	DE  = address to print to (normal spectrum screen)
;
; ******************************************************************************
PrintHex:	
			push	bc
			push	hl
			push	af
			ld		bc,HexCharset

			srl		a
			srl		a
			srl		a
			srl		a	
			call	DrawCharacter	; left nibble

			pop		af
			and		$f	
			call	DrawCharacter	; right nibble
			pop		hl
			pop		bc
			ret




; ******************************************************************************
;
;	Draw a character to the ULA screen
; 	A	= hex value to print
;	DE  = address to print to (normal spectrum screen)
;	BC	= base of character set
;	returns updated DE
;
; ******************************************************************************
DrawCharacter:
			push	hl
			ld		h,0
			ld		l,a
			add		hl,hl	;*8
			add		hl,hl
			add		hl,hl
			add		hl,bc	; add on base of character set

			push	de
			push	bc
			ld		b,8
@lp1:		ld		a,(hl)
			ld		(de),a
			inc		hl		; can't be sure it's on a 256 byte boundary
			inc		d		; next line down
			djnz	@lp1
			pop		bc
			pop		de
			inc		e
			pop		hl
			ret


HexCharset:
		db %00000000	;char30  '0'
		db %00111100
		db %01000110
		db %01000110
		db %01100110
		db %01100110
		db %00111100
		db %00000000
		db %00000000	;char31	'1'
		db %00011000
		db %00101000
		db %00001000
		db %00011000
		db %00011000
		db %00111110
		db %00000000
		db %00000000	;char32	'2'
		db %00111100
		db %01000110
		db %00000110
		db %00111100
		db %01100000
		db %01111110
		db %00000000
		db %00000000	;char33	'3'
		db %00111100
		db %01000110
		db %00001100
		db %00000110
		db %01100110
		db %00111100
		db %00000000
		db %00000000	;char34	'4'
		db %00001100
		db %00011100
		db %00101100
		db %01001100
		db %01111110
		db %00001100
		db %00000000
		db %00000000	;char35	'5'
		db %01111110
		db %01000000
		db %01111100
		db %00000110
		db %01000110
		db %00111100
		db %00000000
		db %00000000	;char36	'6'
		db %00111100
		db %01000000
		db %01111100
		db %01000110
		db %01000110
		db %00111100
		db %00000000
		db %00000000	;char37	'7'
		db %01111110
		db %00000110
		db %00001100
		db %00001000
		db %00011000
		db %00011000
		db %00000000
		db %00000000	;char38	'8'
		db %00111100
		db %01000110
		db %00111100
		db %01000110
		db %01100110
		db %00111100
		db %00000000
		db %00000000	;char39	'9'
		db %00111100
		db %01000110
		db %01000110
		db %00111110
		db %00000110
		db %00111100
		db %00000000
AlphaCharset:
		db %00000000	;char41	'A'
		db %00111100
		db %01000110
		db %01000110
		db %01111110
		db %01100110
		db %01100110
		db %00000000
		db %00000000	;char42	'B'
		db %01111100
		db %01000110
		db %01111100
		db %01000110
		db %01100110
		db %01111100
		db %00000000
		db %00000000	;char43	'C'
		db %00111100
		db %01000110
		db %01000000
		db %01100000
		db %01100110
		db %00111100
		db %00000000
		db %00000000	;char44	'D'
		db %01111000
		db %01001100
		db %01000110
		db %01100110
		db %01101100
		db %01111000
		db %00000000
		db %00000000	;char45	'E'
		db %01111110
		db %01000000
		db %01111100
		db %01100000
		db %01100000
		db %01111110
		db %00000000
		db %00000000	;	'F'
		db %01111110
		db %01000000
		db %01111100
		db %01100000
		db %01100000
		db %01100000
		db %00000000
		db %00000000	;	'G'
		db %00111100
		db %01000110
		db %01000000
		db %01001110
		db %01100110
		db %01111100
		db %00000000
		db %00000000	;	'H'
		db %01000110
		db %01000110
		db %01111110
		db %01100110
		db %01100110
		db %01100110
		db %00000000
		db %00000000	;	'I'
		db %00111110
		db %00011000
		db %00011000
		db %00011000
		db %00011000
		db %00111110
		db %00000000
		db %00000000	;	'J'
		db %00111110
		db %00000110
		db %00000110
		db %00000110
		db %01100110
		db %00111100
		db %00000000
		db %00000000	;	'K'
		db %01000110
		db %01001100
		db %01111000
		db %01101100
		db %01100110
		db %01100110
		db %00000000
		db %00000000	;	'L'
		db %01000000
		db %01000000
		db %01000000
		db %01100000
		db %01100000
		db %01111110
		db %00000000
		db %00000000	;	'M'
		db %01100110
		db %01011110
		db %01011010
		db %01000110
		db %01100110
		db %01100110
		db %00000000
		db %00000000	;	'N'
		db %01100110
		db %01010010
		db %01011010
		db %01011110
		db %01001110
		db %01100110
		db %00000000
		db %00000000	;	'O'
		db %00111100
		db %01000110
		db %01000110
		db %01100110
		db %01100110
		db %00111100
		db %00000000
		db %00000000	;	'P'
		db %01111100
		db %01000110
		db %01111100
		db %01100000
		db %01100000
		db %01100000
		db %00000000
		db %00000000	;	'Q'
		db %00111100
		db %01000110
		db %01000110
		db %01100110
		db %01101100
		db %00111110
		db %00000000
		db %00000000	;	'R'
		db %01111100
		db %01000110
		db %01111100
		db %01110000
		db %01101100
		db %01100110
		db %00000000
		db %00000000	;	'S'
		db %00111100
		db %01000110
		db %00110000
		db %00011100
		db %01000110
		db %00111100
		db %00000000
		db %00000000	;	'T'
		db %01111110
		db %00011000
		db %00011000
		db %00011000
		db %00011000
		db %00011000
		db %00000000
		db %00000000	;	'U'
		db %01000110
		db %01000110
		db %01000110
		db %01100110
		db %01100110
		db %00111100
		db %00000000
		db %00000000	;	'V'
		db %01000110
		db %01000110
		db %01000110
		db %01100110
		db %00101100
		db %00011000
		db %00000000
		db %00000000	;	'W'
		db %01000110
		db %01000110
		db %01000110
		db %01010110
		db %01111110
		db %01100110
		db %00000000
		db %00000000	;	'X'
		db %01000110
		db %00101100
		db %00011000
		db %00101100
		db %01100110
		db %01100110
		db %00000000
		db %00000000	;	'Y'
		db %01000110
		db %00101100
		db %00011000
		db %00011000
		db %00011000
		db %00011000
		db %00000000
		db %00000000	;	'Z'
		db %01111110
		db %00001100
		db %00011000
		db %00110000
		db %01100000
		db %01111110
		db %00000000
		db %00000000	;	'[' for the lifes left display
		db %01101100
		db %01101100
		db %01101100
		db %01111100
		db %11111110
		db %11000110
		db %00000000
		db %00000000	;	'\' for the lifes left display
		db %00000000
		db %00000000
		db %00000000
		db %00000000
		db %00000000
		db %00000000
		db %00000000



