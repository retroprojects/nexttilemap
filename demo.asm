; *****************************************************************************************************************************
;	Spectrum Next Tilemap Example Code
; 
;	Created by Thomas Sturm, https://sturm.to - https://toot.site/@tsturm
; 
; 	This code is free to use without restrictions
; 
;	v1.0
; 
;	Assembler directives are for sjasmplus - https://github.com/z00m128/sjasmplus/
; 
; *****************************************************************************************************************************


; *****************************************************************************************************************************
; assembler setup
; *****************************************************************************************************************************
  			  	DEVICE ZXSPECTRUMNEXT
			    OPT		--zxnext

				MMU 0 1, 0		; map slots 0 & 1 with RAM bank 0



;----------------------------------------------------------------------------------------------------------------------
; Memory map
;									bank	slot
; $0000         RAM					0		0
; $2000         							1
; $4000         Tilemap	(5120B)		5		2
; $6000         Tiles (4K)					3
; $7000			unused
; $8000         Code (8K)			2		4
; $a000										5
; $c000			Textbuffer (8K)		1		6
; $e000										7

; *****************************************************************************************************************************
; Font
; *****************************************************************************************************************************

        		org 0x6000

TrueFont
				ds 4096




; *****************************************************************************************************************************
; code start
; *****************************************************************************************************************************

		    	ORG 0x8000
start  
				ei


; *****************************************************************************************************************************
; general setup
; *****************************************************************************************************************************


				call	fontSetup


; *****************************************************************************************************************************
; prepare screen and hardware
; *****************************************************************************************************************************

				nextreg $08,64		; disable RAM and I/O port contention

				; Peripheral 1 setting
									; 001 = Kempston 1 (port 0x1F)
									; 100 = Kempston 2 (port 0x37)
									; bits 7-6 = joystick 1 mode (LSB)
									; bits 5-4 = joystick 2 mode (LSB)
									; bit 3 = joystick 1 mode (MSB)
									; bit 2 = 50/60 Hz mode (0 = 50Hz, 1 = 60Hz)(0 after a PoR or Hard-reset)
									; bit 1 = joystick 2 mode (MSB)
									; bit 0 = Enable Scandoubler (1 = enabled)(1 after a PoR or Hard-reset)
									; 01 00 0 0 1 1 (67)
;				nextreg $05,67

				; set the graphic layer priority
									; sprite and layers
									; bit 7 low res mode
									; bit 6 sprite priority 1=sprite 0 on top
									; bit 5 sprite clipping over border
									; bit 4-2 
									;	000: sprites over layer 2 over ula
									;	100: ula over sprites over layer 2
									; bit 1 over border mode
									; bit 0 sprites visible
;				nextreg $15,17		; 0 0 0 100 0 1 (17)


				call	InitTilemap	; setup tilemap mode 



; *****************************************************************************************************************************
; application start
; *****************************************************************************************************************************


startMainLoop	ld 		hl,pretim	; preset frame timer (in utils.asm)
				ld 		a,(FRAMES)	; Spectrum Frame Counter
				ld 		(hl),a


				ld 		hl,textBufLine
;				ld		(hl),0

				ld		(hl),0		; initial first line of the text buffer to show in the tilemap

				inc 	hl
				ld		(hl),$0000

; -----------------

; TEMP TEMP TEMP
; TEMP TEMP TEMP
; TEMP TEMP TEMP

; copy demo text to text buffer until we hit 0
				ld		de,DemoText
				ld		hl,TextBuffer

.setupLoop1		ld      a,(de)
                and     a
                jr      z,.loop1End
                ld      (hl),a
                inc		de
                inc     hl
				jr      .setupLoop1
.loop1End
				ld      (hl),0			; add the final 0 to the text buffer


; we write out two lines at the top that won't scroll with the rest of the demo text
; tilemap print
;       B = Y coord (0-31)
;       C = X coord (0-79)
;       DE = string
;       A = colour (0-15)

				ld		b,0
				ld		c,0
				ld		a,0
				ld		de,TextHead1
				call	TilePrintString
				ld		b,1
				ld		c,0
				ld		a,0
				ld		de,TextHead2
				call	TilePrintString


; show the first page of the text buffer
				call	calcBuffOffset
				ld		de,TextBuffer
				call	TilePrintBuffer



; TEMP TEMP TEMP
; TEMP TEMP TEMP
; TEMP TEMP TEMP


; *****************************************************************************************************************************
; *****************************************************************************************************************************
; *****************************************************************************************************************************
; main loop
; *****************************************************************************************************************************
; *****************************************************************************************************************************
; *****************************************************************************************************************************

mainloop		

@updateBuffer

				ld		hl,textBufLineAdr
				ld		de,(hl)

;				ld		d,(hl)
;				ld		e,80
;				mul								; de=d*e

				ld		hl,TextBuffer
				add		hl,de
				ld		de,hl

				call	TilePrintBuffer



; we check the keyboard in the loop - "Z" scrolls the text up, "A" scrolls the text down
@keyboardHandler
                call    ReadKeyboard

                ld      a,(Keys+VK_Z)           ; key "Z"
                and     $ff
                jr      z,@znotpressed

				ld		hl,textBufLine
				ld		a,(hl)
; here we should probably do something if the displayed text goes beyond the existing text in the text buffer
				inc		a
				ld		(hl),a
				call	calcBuffOffset

@znotpressed	ld      a,(Keys+VK_A)           ; key "A"
                and     $ff
                jr      z,@anotpressed

				ld		hl,textBufLine
				ld		a,(hl)
				cp		0
				jr		z,@anotpressed			; reached the top of the buffer
				dec		a
				ld		(hl),a
				call	calcBuffOffset

@anotpressed
				jp 		mainloop
				
; *****************************************************************************************************************************
; *****************************************************************************************************************************
; *****************************************************************************************************************************



; calculate the byte offset in TextBuffer for the first line of the text to display
calcBuffOffset	push	hl
				push	de
				push	bc

				ld 		hl,textBufLine
				ld		b,(hl)
				
				ld		hl,TextBuffer
				ld		de,0

				ld		a,b
				cp		0
				jr		z,@nocalc			; if we print line 0, we skip all of this and save 0 as offset

				ld		c,0

@lineloop		ld		a,(hl)
				inc		hl
				inc		c
				inc		de

				cp		13					; CR, so count down lines
				jr		z,@lineEndLine

				ld		a,C
				cp		80
				jr		nz,@lineloop		; not all the way to the right, edge, keep going

@lineEndLine	ld		c,0
@calcDone		djnz	@lineloop			; count down the number of lines we are trying to skip

@nocalc			ld		hl,textBufLineAdr
				ld		(hl),de

				pop		bc
				pop		de
				pop		hl
				ret


; *****************************************************************************************************************************
; program data and constants
; *****************************************************************************************************************************

textBufLine		db		0		; first line of the text buffer to be shown on the tilemap
textBufLineAdr	dw		0		; the address offset to TextBuffer of the beginning of the first line based on textBufLine


;				Useful System Addresses
SpritePattern	equ		$5b
NextSet			equ		$243b
NextVal			equ		$253b
FRAMES			equ		$5c78	; Spectrum Frame Counter, updated every 20ms by VBL
Kempston1 		equ		$1F
Kempston2		equ		$37




; -----------------

TextHead1		db		"Spectrum Next Tilemap Demo by Thomas Sturm - A and Z to scroll"
				db		0
TextHead2		db		"abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890 !@#$%^&*()[]{}-"
				db		0


DemoText		db		"1 We love the ZX Spectrum. Why wouldn't we? It was much more than just a computer: it was "
				db		"a machine that sparked a gaming revolution, neatly housed within its iconic design "
				db		"powered by sheer simplicity. The Speccy was, and still is, on a league of its own."
				db		13
				db		13
				db		"2 Decades have come and gone and the Speccy is still alive and kicking. New games are "
				db		"being launched all the time, the demoscene carries on pushing the hardware limits to "
				db		"the unimaginable, artists keep on creating great 8-bit eyecandy and music with it. "
				db		"Alongside this there are thousands of awesome games in the back catalogue to play."
				db		13
				db		13
				db		"3 Meanwhile hardware hackers around the world have expanded the ZX Spectrum "
				db		"to support SD card storage, feature new and better video modes, pack more memory, "
				db		"faster processor... Problem is, these expansions can be difficult to get hold "
				db		"of, and without a standardised Spectrum, no one knows what to support or develop for."
				db		13
				db		13
				db		"4 Here is our answer: The Spectrum Next - an updated and enhanced version of "
				db		"the ZX Spectrum totally compatible with the original, featuring the major "
				db		"hardware developments of the past many years packed inside a simple (and beautiful) "
				db		"design by the original designer, Rick Dickinson, inspired by his seminal "
				db		"work at Sinclair Research."
				db		13
				db		13
				db		"5 Decades have come and gone and the Speccy is still alive and kicking. New games are "
				db		"being launched all the time, the demoscene carries on pushing the hardware limits to "
				db		"the unimaginable, artists keep on creating great 8-bit eyecandy and music with it. "
				db		"Alongside this there are thousands of awesome games in the back catalogue to play."
				db		13
				db		13
				db		"6 Meanwhile hardware hackers around the world have expanded the ZX Spectrum "
				db		"to support SD card storage, feature new and better video modes, pack more memory, "
				db		"faster processor... Problem is, these expansions can be difficult to get hold "
				db		"of, and without a standardised Spectrum, no one knows what to support or develop for."
				db		13
				db		13
				db		"7 Here is our answer: The Spectrum Next - an updated and enhanced version of "
				db		"the ZX Spectrum totally compatible with the original, featuring the major "
				db		"hardware developments of the past many years packed inside a simple (and beautiful) "
				db		"design by the original designer, Rick Dickinson, inspired by his seminal "
				db		"work at Sinclair Research."
				db		13
				db		13
				db		"8 Decades have come and gone and the Speccy is still alive and kicking. New games are "
				db		"being launched all the time, the demoscene carries on pushing the hardware limits to "
				db		"the unimaginable, artists keep on creating great 8-bit eyecandy and music with it. "
				db		"Alongside this there are thousands of awesome games in the back catalogue to play."
				db		13
				db		13
				db		"9 Meanwhile hardware hackers around the world have expanded the ZX Spectrum "
				db		"to support SD card storage, feature new and better video modes, pack more memory, "
				db		"faster processor... Problem is, these expansions can be difficult to get hold "
				db		"of, and without a standardised Spectrum, no one knows what to support or develop for."
				db		13
				db		13
				db		"10 Here is our answer: The Spectrum Next - an updated and enhanced version of "
				db		"the ZX Spectrum totally compatible with the original, featuring the major "
				db		"hardware developments of the past many years packed inside a simple (and beautiful) "
				db		"design by the original designer, Rick Dickinson, inspired by his seminal "
				db		"work at Sinclair Research."
				db 		0

; -----------------




; *****************************************************************************************************************************
; include modules
; *****************************************************************************************************************************
				include "utils.asm"
				include "tiletool.asm"
				include "font.asm"




; *****************************************************************************************************************************
; Textbuffer
; *****************************************************************************************************************************

        		org 0xc000

TextBuffer
				ds 8192




; *****************************************************************************************************************************
; save as .NEX file (sjasm specific pseudocode)
; *****************************************************************************************************************************

				SAVENEX OPEN "tilemap.nex", start, $FF00
				SAVENEX CORE 2, 0, 0        ; Next core 2.0.0 required as minimum
				SAVENEX CFG 7, 0            ; white border, file handle in BC
				SAVENEX BANK 5, 2, 0, 1, 3, 4, 6, 7    	; store the 16Kbyte banks
				SAVENEX CLOSE
				
				